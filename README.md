# EOLE OpenNebula tools

This is a collection of useful tools to maintain a running OpenNebula instance.

## `clean-done-vms`

When a virtual machine reach the `DONE` state, the directory
`/var/lib/one/vms/<VMID>` and the log file `/var/lib/one/<VMID>.log`
are [not removed for auditing purpose](https://github.com/OpenNebula/one/issues/2408).

The `clean-done-vms` script will cleanup them in two cases:

- for VM in state `DONE` for longer than a configurable time (30 days
  by default)
- for directories and files not associated with any known VMs in
  OpenNebula database (probably already purged during `onedb purge-done`)

This tool can be added to the `crontab` of the user `oneadmin`, for
example:

```
@monthly ${HOME}/tools/clean-done-vms --verbose --no-dry-run | mail -s 'OpenNebula cleanup' -r opennebula-tools@example.net sysadmins@example.net
```

### Options

By default, the tool only shows what would be done.

<dl>
  <dt><code>-t</code>, <code>--time TIME</code></dt>
  <dd>Cleanup VMs older than this date, default to the current date
      minus 30 days, at midnight</dd>
  <dt><code>--no-directory</code></dt>
  <dd>Do not cleanup the VMs directories under <code>/var/lib/one/vms</code></dd>
  <dt><code>--no-log-file</code></dt>
  <dd>Do not cleanup the VMs log file under <code>/var/log/one</code></dd>
  <dt><code>--no-db</code></dt>
  <dd>Do not cleanup the VMs in <code>DONE</code> state in the OpenNebula database</dd>
  <dt><code>-v</code>, <code>--verbose</code></dt>
  <dd>Show verbose logs, one line per removed file and directory</dd>
  <dt><code>-d</code>, <code>--debug</code></dt>
  <dd>Show debug logs</dd>
  <dt><code>--no-dry-run</code></dt>
  <dd>Do not run in dry-run mode</dd>
</dl>

When executing the command with `--no-dry-run` to actually cleanup
things, there is no output by default. You need to add `--verbose` to
have output of what is done.

If you use `--debug`, the tool will display one line per file not
removed with the reason why, it implies `--verbose`.
